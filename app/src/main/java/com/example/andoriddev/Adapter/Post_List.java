package com.example.andoriddev.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.andoriddev.Extra.Post;
import com.example.andoriddev.R;

import java.util.List;

/**
 *Post_List - A list adapter for the posts in main page
 *
 * @author William Rich
 * @version v0.5
 * @since 7 September 2019
 */
public class Post_List extends ArrayAdapter<Post> {

    private Activity context;
    private List<Post> postList;

    public Post_List(Activity context, List<Post> postlist) {
        super(context, R.layout.post_item_list, postlist);
        this.context = context;
        this.postList = postlist;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.post_item_list, parent, false);

        Post post = postList.get(position);

        TextView textViewUser = (TextView) listViewItem.findViewById(R.id.textview_post_item_list_user);
        TextView textViewTitle = (TextView) listViewItem.findViewById(R.id.textview_post_item_list_title);
        TextView textViewUserType = (TextView) listViewItem.findViewById(R.id.textview_post_user_type);

        textViewUser.setText("@" + post.getName().toString());
        textViewTitle.setText(post.getPostTitle().toString());
        textViewUserType.setText(post.getUserType().toString());

        return listViewItem;
    }
}
