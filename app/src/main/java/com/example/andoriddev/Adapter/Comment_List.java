package com.example.andoriddev.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.example.andoriddev.Extra.Comment;
import com.example.andoriddev.R;

import org.w3c.dom.Text;

import java.util.List;

public class Comment_List extends RecyclerView.Adapter<Comment_List.ViewHolder> {

    private Context context;
    private List<Comment> commentList;

    public Comment_List(Context context, List<Comment> commentList) {
        this.context = context;
        this.commentList = commentList;
    }

//    @NonNull
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        ViewHolder viewHolder;
//
//
////        LayoutInflater inflater = context.getLayoutInflater();
//
////        View listViewItem = inflater.inflate(R.layout.comment_list_item,null, true);
//
//        Comment comment = commentList.get(position);
//
//
//        viewHolder.commentUserTxt.setText("@"+comment.getCommentUser().toString());
//        viewHolder.commentContent.setText(comment.getCommentContent().toString());
//
//        return listViewItem;
//    }

    @NonNull
    @Override
    public Comment_List.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.comment_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Comment_List.ViewHolder holder, int position) {
        Comment comment = commentList.get(position);

        holder.commentUserTxt.setText("@" + comment.getCommentUser().toString());
        holder.commentContent.setText(comment.getCommentContent().toString());
    }

    @Override
    public int getItemCount() {
        return this.commentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView commentUserTxt;
        private TextView commentContent;

        public ViewHolder(@NonNull View view) {
            super(view);
            this.commentUserTxt = (TextView) view.findViewById(R.id.textview_comment_list_user);
            this.commentContent = (TextView) view.findViewById(R.id.textview_comment_list_comment);
        }
    }
}
