package com.example.andoriddev;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.andoriddev.Extra.Admin;
import com.example.andoriddev.Extra.Post;
import com.example.andoriddev.Extra.Premium;
import com.example.andoriddev.Extra.Regular;
import com.example.andoriddev.Extra.Upload;
import com.example.andoriddev.Extra.Users;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * PostActivity.java - Activity to allow user creating a post
 *
 * @author William Rich
 * @version v0.5
 * @since 7 September 2019
 */
public class PostActivity extends AppCompatActivity {

    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1001;
    private static final int RECORD_CODE = 1003;
    private static final String LOG_TAG = "Record_log";
    private EditText title, content;
    private TextView recordText;
    private Button cancel, post;
    private FloatingActionButton fabImage, fabCamera, fabMic, audioPlay, audioStop;
    private ImageView imageViewPicture;

    private Uri filePath;
    private Uri image_uri;

    private MediaRecorder mRecorder;
    private MediaPlayer mediaPlayer;
    private String mFilename = null;
    private String audioPath = null;
    private String pushIdKey;

    private FirebaseStorage storage;
    private StorageReference storageReference, audioReference;
    private FirebaseUser firebaseUser;
    private DatabaseReference dbRef, dbRefUsers, dbMyPost;

    private LinearLayout audioLayoutPost;

    String pathSave;

    private final int PICK_IMAGE_REQUEST = 71;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        Toolbar toolbar = findViewById(R.id.toolbar_post);
        toolbar.setTitle("Create a post");
        setSupportActionBar(toolbar);

        recordText = (TextView) findViewById(R.id.textview_post_record);
        title = (EditText) findViewById(R.id.edittext_post_title);
        content = (EditText) findViewById(R.id.edittext_post_content);
        cancel = (Button) findViewById(R.id.button_cancel);
        post = (Button) findViewById(R.id.button_post);
        fabImage = (FloatingActionButton) findViewById(R.id.fab_post_picture);
        fabCamera = (FloatingActionButton) findViewById(R.id.fab_post_camera);
        fabMic = (FloatingActionButton) findViewById(R.id.fab_post_microphone);
        audioPlay = (FloatingActionButton) findViewById(R.id.audio_play_post);
        audioStop = (FloatingActionButton) findViewById(R.id.audio_stop_post);
        imageViewPicture = (ImageView) findViewById(R.id.imageView_picture);
        audioLayoutPost = (LinearLayout) findViewById(R.id.audio_layout_post);


        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        dbRef = FirebaseDatabase.getInstance().getReference("Post List");
        dbRefUsers = FirebaseDatabase.getInstance().getReference("Users");
        dbMyPost = FirebaseDatabase.getInstance().getReference("My Post");

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference("PostImage");
        audioReference = storage.getReference("PostAudio");


        mFilename = Environment.getExternalStorageDirectory().getAbsolutePath();
        mFilename += "/recorded_audio.3gp";

        pushIdKey = dbRef.push().getKey();

        pathSave = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + pushIdKey.toString() + "_audio_record.3gp";

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostActivity.this, MainActivity.class));
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post();
            }
        });

        fabImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        fabMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(getApplicationContext(), RecordActivity.class);
                activityIntent.putExtra("pushKey", pushIdKey.toString());
                Toast.makeText(getApplicationContext(), pushIdKey, Toast.LENGTH_SHORT).show();
                startActivityForResult(activityIntent, RECORD_CODE);
                Toast.makeText(getApplicationContext(), audioPath, Toast.LENGTH_SHORT).show();
            }
        });

        audioPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordText.setText("Playing...");
                playAudioFromUrl(pathSave);
            }
        });

        audioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    recordText.setText("TAP 'PLAY' to play audio");
                }
            }
        });
    }

    private void uploadAudio(String pushId2) {
        final Uri uriAudio = Uri.fromFile(new File(audioPath).getAbsoluteFile());
        final String pushId = pushId2;
        final StorageReference audioRef = audioReference.child(pushId);

        audioRef.putFile(uriAudio).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> urlTask = audioRef.putFile(uriAudio).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return audioRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "AudioPost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostAudio").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
                Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadImage(String pushId2) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        final String pushId = pushId2;
        final StorageReference ref = storageReference.child(pushId);
        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                Task<Uri> urlTask = ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "ImagePost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });

                Toast.makeText(getApplicationContext(), "Image Uploaded", Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.setMessage("Uploading...");
                    }
                });

    }

    private void uploadCamera(String pushId2) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        final String pushId = pushId2;
        String pushKey = "img" + UUID.randomUUID().toString();
        final StorageReference ref = storageReference.child(pushId);
        ref.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                Task<Uri> urlTask = ref.putFile(image_uri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "ImagePost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
                Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.setMessage("Uploading...");
                    }
                });
    }

    //get image from storage
    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageViewPicture.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == IMAGE_CAPTURE_CODE && resultCode == RESULT_OK) {
            imageViewPicture.setImageURI(image_uri);

        }

        if (requestCode == RECORD_CODE && resultCode == RESULT_OK) {
            recordText.setText("TAP 'PLAY Button' to play audio");
            recordText.setVisibility(View.VISIBLE);
            audioPath = data.getData().toString();
            audioLayoutPost.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), audioPath, Toast.LENGTH_LONG).show();
        }
    }

    public void emptyFieldAlert() {
        AlertDialog.Builder y = new AlertDialog.Builder(PostActivity.this);

        y.setTitle("EMPTY VALUE");
        y.setMessage("All fields is necessary");
        y.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = y.create();
        alert.show();
    }

    //Get image uri from the camera
    public void takePicture() {
        //system os more then marshmallow, request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE);
            } else {
                //permission already granted
                openCamera();
            }
        } else {
            //sytem os<marsmallow
            openCamera();
        }
    }

    //open the camera
    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        //Camera intent
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);
    }

    //handling permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup was granted
                    openCamera();
                } else {
                    //permission from popup was denied
                    Toast.makeText(getApplicationContext(), "Permission denied...", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    //Record voice function
    private void playAudioFromUrl(String fileUrl) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(fileUrl);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    recordText.setText("TAP 'PLAY Button' to play audio");
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void post() {
        String title_txt = title.getText().toString();
        String content_txt = content.getText().toString();
        if (title_txt.isEmpty() || content_txt.isEmpty()) {
            emptyFieldAlert();
        } else {
            dbRefUsers.child("REGULAR").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                        Users users = userDetailsSnapshot.getValue(Regular.class);
                        if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                            String title_txt = title.getText().toString();
                            String content_txt = content.getText().toString();
                            String pushId = users.priority() + pushIdKey;
                            final Post post = new Post(title_txt, content_txt, firebaseUser.getUid().toString(), users.getName().toString(), pushId, users.getUserType());
                            dbRef.child(pushId).setValue(post);
                            dbMyPost.child(firebaseUser.getUid().toString()).child(pushId).setValue(post);
                            if (!(filePath == null)) {
                                uploadImage(pushId);
                            }
                            if (!(image_uri == null)) {
                                uploadCamera(pushId);
                            }
                            if (!(audioPath == null)) {
                                uploadAudio(pushId);
                            }
                            startActivity(new Intent(PostActivity.this, MainActivity.class));
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            dbRefUsers.child("PREMIUM").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                        Users users = userDetailsSnapshot.getValue(Premium.class);
                        if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                            String title_txt = title.getText().toString();
                            String content_txt = content.getText().toString();
                            String pushId = users.priority() + pushIdKey;
                            final Post post = new Post(title_txt, content_txt, firebaseUser.getUid().toString(), users.getName().toString(), pushId, users.getUserType());
                            dbRef.child(pushId).setValue(post);
                            dbMyPost.child(firebaseUser.getUid().toString()).child(pushId).setValue(post);
                            if (!(filePath == null)) {
                                uploadImage(pushId);
                            }
                            if (!(image_uri == null)) {
                                uploadCamera(pushId);
                            }
                            if (!(audioPath == null)) {
                                uploadAudio(pushId);
                            }
                            startActivity(new Intent(PostActivity.this, MainActivity.class));
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            dbRefUsers.child("ADMIN").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                        Users users = userDetailsSnapshot.getValue(Admin.class);
                        if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                            String title_txt = title.getText().toString();
                            String content_txt = content.getText().toString();
                            String pushId = users.priority() + pushIdKey;
                            final Post post = new Post(title_txt, content_txt, firebaseUser.getUid().toString(), users.getName().toString(), pushId, users.getUserType());
                            dbRef.child(pushId).setValue(post);
                            dbMyPost.child(firebaseUser.getUid().toString()).child(pushId).setValue(post);
                            if (!(filePath == null)) {
                                uploadImage(pushId);
                            }
                            if (!(image_uri == null)) {
                                uploadCamera(pushId);
                            }
                            if (!(audioPath == null)) {
                                uploadAudio(pushId);
                            }
                            startActivity(new Intent(PostActivity.this, MainActivity.class));

                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }
}
