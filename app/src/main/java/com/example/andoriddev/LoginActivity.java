package com.example.andoriddev;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

/**
 * Login functions
 * Takes in Email (with @email.com format) and password (minimal 6 characters)
 *
 * @author William Rich
 * @version v0.2
 * @since 5 August 2019
 */

public class LoginActivity extends AppCompatActivity {
    EditText email, password;
    Button btn_login;
    private ProgressDialog mProgressDialogLogin;

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        Toolbar toolbar = findViewById(R.id.toolbar_login);
        toolbar.setTitle("Login");
        setSupportActionBar(toolbar);

        auth = FirebaseAuth.getInstance();

        email = findViewById(R.id.edittext_email);
        password = findViewById(R.id.edittext_password);
        btn_login = findViewById(R.id.button_login2);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_email = email.getText().toString();
                String txt_password = password.getText().toString();

                if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)) {
                    Toast emptyFields = Toast.makeText(LoginActivity.this, "All fields are required", Toast.LENGTH_LONG);
                    emptyFields.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                    emptyFields.show();
                } else {
                    auth.signInWithEmailAndPassword(txt_email, txt_password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(loginIntent);
                                finish();
                            } else {
                                Toast failedAuth = Toast.makeText(LoginActivity.this, "Failed Authenthication", Toast.LENGTH_LONG);
                                failedAuth.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                                failedAuth.show();
                            }
                        }
                    });
                }
            }
        });
    }

    private void showProgressDialogLogin() {
        if (mProgressDialogLogin == null) {
            mProgressDialogLogin = new ProgressDialog(this);
            mProgressDialogLogin.setMessage("Loading");
            mProgressDialogLogin.setIndeterminate(true);
            mProgressDialogLogin.dismiss();
        }
        mProgressDialogLogin.show();
    }
}
