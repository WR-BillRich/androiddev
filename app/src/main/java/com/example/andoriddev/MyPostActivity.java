package com.example.andoriddev;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.andoriddev.Adapter.Post_List;
import com.example.andoriddev.Extra.Post;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ViewCommmentActivity.java - Activity to allow user view a post and comments
 *
 * @author William Rich
 * @version v0.6
 * @since 8 September 2019
 */
public class MyPostActivity extends AppCompatActivity {

    public static final String KEY_MYPOST = "key_mypost";

    List<Post> postList;
    DatabaseReference dbRefMyPost;
    ListView listViewMyPost;
    FirebaseUser firebaseUser;
    FloatingActionButton fabBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post);

        Toolbar toolbar = findViewById(R.id.toolbar_mypost);
        toolbar.setTitle("My Post");
        setSupportActionBar(toolbar);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        listViewMyPost = (ListView) findViewById(R.id.listview_mypost);
        dbRefMyPost = FirebaseDatabase.getInstance().getReference("My Post");
        postList = new ArrayList<>();

        dbRefMyPost.child(firebaseUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                postList.clear();

                for (DataSnapshot myPostDataSnapshot : dataSnapshot.getChildren()) {
                    Post post = myPostDataSnapshot.getValue(Post.class);
                    postList.add(0, post);
                }
                Post_List listAdapter = new Post_List(MyPostActivity.this, postList);
                listViewMyPost.setAdapter(listAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        fabBack = (FloatingActionButton) findViewById(R.id.fab_mypost_back);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });

        listViewMyPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Post selectedPost = postList.get(position);

                Intent intent = new Intent(getApplicationContext(), MyPostDeleteEditActivity.class);
                intent.putExtra(KEY_MYPOST, (Serializable) selectedPost);
                startActivity(intent);
            }
        });
    }
}
