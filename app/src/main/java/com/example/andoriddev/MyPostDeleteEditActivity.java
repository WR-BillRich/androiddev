package com.example.andoriddev;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andoriddev.Extra.Admin;
import com.example.andoriddev.Extra.Post;
import com.example.andoriddev.Extra.Premium;
import com.example.andoriddev.Extra.Regular;
import com.example.andoriddev.Extra.Upload;
import com.example.andoriddev.Extra.Users;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class MyPostDeleteEditActivity extends AppCompatActivity {

    public static final String KEY_MYPOST = "key_mypost";
    Users currentUser;
    Button delete, save, cancel;
    EditText title, content;


    FloatingActionButton camera, record, image, audioPlay, audioStop;
    ImageView imageView;
    TextView audioText;
    LinearLayout audioLayout;
    MediaPlayer mediaPlayer;

    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1001;
    private static final int RECORD_CODE = 1003;
    private final int PICK_IMAGE_REQUEST = 71;

    private FirebaseStorage storage;
    private StorageReference storageReference, audioReference;

    private Uri filePath;
    private Uri image_uri;
    String pathSave;
    private String audioPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_post_delete_edit);

        Toolbar toolbar = findViewById(R.id.mypost_toolbar);
        toolbar.setTitle("My Post");
        setSupportActionBar(toolbar);

        final Post post = (Post) getIntent().getSerializableExtra(KEY_MYPOST);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference("PostImage");
        audioReference = storage.getReference("PostAudio");

        delete = (Button) findViewById(R.id.mypost_button_delete);
        save = (Button) findViewById(R.id.mypost_button_save);
        cancel = (Button) findViewById(R.id.mypost_button_cancel);
        title = (EditText) findViewById(R.id.mypost_edittext_post_title);
        content = (EditText) findViewById(R.id.mypost_edittext_post_content);

        camera = (FloatingActionButton) findViewById(R.id.fab_mypost_camera);
        record = (FloatingActionButton) findViewById(R.id.fab_mypost_microphone);
        image = (FloatingActionButton) findViewById(R.id.fab_mypost_picture);

        imageView = (ImageView) findViewById(R.id.imageView_picture_mypost);
        audioText = (TextView) findViewById(R.id.textview_mypost_record);

        audioLayout = (LinearLayout) findViewById(R.id.audio_layout_mypost);
        audioPlay = (FloatingActionButton) findViewById(R.id.audio_play_mypost);
        audioStop = (FloatingActionButton) findViewById(R.id.audio_stop_mypost);

        pathSave = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + post.getPostId().toString() + "_audio_record.3gp";

        title.setText(post.getPostTitle().toString());
        content.setText(post.getPostContent().toString());

        DatabaseReference dbRefUsers = FirebaseDatabase.getInstance().getReference("Users");

        dbRefUsers.child("REGULAR").addValueEventListener(new ValueEventListener() {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                    Users users = userDetailsSnapshot.getValue(Regular.class);
                    if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                        setCurrentUser(users);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefUsers.child("PREMIUM").addValueEventListener(new ValueEventListener() {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                    Users users = userDetailsSnapshot.getValue(Premium.class);
                    if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                        setCurrentUser(users);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefUsers.child("ADMIN").addValueEventListener(new ValueEventListener() {
            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                    Users users = userDetailsSnapshot.getValue(Admin.class);
                    if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                        setCurrentUser(users);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MyPostActivity.class));
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser.deleteRights()) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    DatabaseReference dbRefPost = FirebaseDatabase.getInstance().getReference("Post List").child(post.getPostId().toString());
                    DatabaseReference dbRefMyPost = FirebaseDatabase.getInstance().getReference("My Post").child(user.getUid().toString()).child(post.getPostId().toString());
                    DatabaseReference dbRefComment = FirebaseDatabase.getInstance().getReference("Comment").child(post.getPostId().toString());
                    DatabaseReference dbRefUploads = FirebaseDatabase.getInstance().getReference("Uploads");
                    dbRefUploads.child("PostAudio").child(post.getPostId().toString()).removeValue();
                    dbRefUploads.child("PostImage").child(post.getPostId().toString()).removeValue();
                    dbRefPost.removeValue();
                    dbRefMyPost.removeValue();
                    dbRefComment.removeValue();
                    storageReference.child(post.getPostId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Delete successfull image", Toast.LENGTH_SHORT);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), "Item does not exist", Toast.LENGTH_SHORT);
                        }
                    });
                    audioReference.child(post.getPostId()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getApplicationContext(), "Delete successfull audio", Toast.LENGTH_SHORT);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), "Item does not exist", Toast.LENGTH_SHORT);
                        }
                    });
                    startActivity(new Intent(getApplicationContext(), MyPostActivity.class));
                } else {
                    deleteAlert();
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser.editRights()) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    String titleText = title.getText().toString();
                    String contentText = content.getText().toString();
                    DatabaseReference dbRefPost = FirebaseDatabase.getInstance().getReference("Post List").child(post.getPostId());
                    DatabaseReference dbRefMyPost = FirebaseDatabase.getInstance().getReference("My Post").child(user.getUid().toString()).child(post.getPostId());
                    Post newPost = new Post(titleText, contentText, post.getUserId(), post.getName(), post.getPostId(), post.getUserType());
                    dbRefPost.setValue(newPost);
                    dbRefMyPost.setValue(newPost);
                    startActivity(new Intent(getApplicationContext(), MyPostActivity.class));
                    if (!(filePath == null)) {
                        uploadImage(post.getPostId());
                    }
                    if (!(image_uri == null)) {
                        uploadCamera(post.getPostId());
                    }
                    if (!(audioPath == null)) {
                        uploadAudio(post.getPostId());
                    }
                } else {
                    editAlert();
                }
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityIntent = new Intent(getApplicationContext(), RecordActivity.class);
                activityIntent.putExtra("pushKey", post.getPostId().toString());
                Toast.makeText(getApplicationContext(), post.getPostId(), Toast.LENGTH_SHORT).show();
                startActivityForResult(activityIntent, RECORD_CODE);
                Toast.makeText(getApplicationContext(), audioPath, Toast.LENGTH_SHORT).show();
            }
        });

        audioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    audioText.setText("TAP 'PLAY' to play audio");
                }
            }
        });

        getImageUrl(post);
        getAudioUrl(post);


    }

    public void setCurrentUser(Users user) {
        currentUser = user;
    }

    public void deleteAlert() {
        AlertDialog.Builder y = new AlertDialog.Builder(MyPostDeleteEditActivity.this);

        y.setTitle("NO RIGHTS");
        y.setMessage("This features is only available for PREMIUM user and ADMIN");
        y.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = y.create();
        alert.show();
    }

    public void editAlert() {
        AlertDialog.Builder y = new AlertDialog.Builder(MyPostDeleteEditActivity.this);

        y.setTitle("NO RIGHTS");
        y.setMessage("This features is only available for PREMIUM user");
        y.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = y.create();
        alert.show();
    }

    private void getImageUrl(final Post post) {
        DatabaseReference dbRefUrl = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage");
        dbRefUrl.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot imageDataSnapshot : dataSnapshot.getChildren()) {
                    Upload uploadChild = imageDataSnapshot.getValue(Upload.class);
                    if (post.getPostId().equals(uploadChild.getPostId())) {
                        setImageFromUrl(uploadChild.getFileUrl());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setImageFromUrl(String fileUrl) {
        Picasso.get().load(fileUrl)
                .error(R.mipmap.ic_launcher)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
    }

    private void getAudioUrl(final Post post) {
        DatabaseReference dbRefUrl = FirebaseDatabase.getInstance().getReference("Uploads").child("PostAudio");
        dbRefUrl.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot audioDataSnapshot : dataSnapshot.getChildren()) {
                    Upload uploadChild = audioDataSnapshot.getValue(Upload.class);
                    if (post.getPostId().equals(uploadChild.getPostId())) {
                        final String audioUrl = uploadChild.getFileUrl();
                        audioLayout.setVisibility(View.VISIBLE);
                        audioText.setText("TAP 'PLAY' to play audio");
                        audioPlay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                audioText.setText("Playing...");
                                playAudioFromUrl(audioUrl);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void playAudioFromUrl(String fileUrl) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(fileUrl);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audioText.setText("TAP 'PLAY' to play audio");
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void uploadAudio(String pushId2) {
        final Uri uriAudio = Uri.fromFile(new File(audioPath).getAbsoluteFile());
        final String pushId = pushId2;
        final StorageReference audioRef = audioReference.child(pushId);

        audioRef.putFile(uriAudio).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Task<Uri> urlTask = audioRef.putFile(uriAudio).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return audioRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "AudioPost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostAudio").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
                Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void uploadImage(String pushId2) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        final String pushId = pushId2;
        final StorageReference ref = storageReference.child(pushId);
        ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                Task<Uri> urlTask = ref.putFile(filePath).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "ImagePost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });

                Toast.makeText(getApplicationContext(), "Image Uploaded", Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.setMessage("Uploading...");
                    }
                });

    }

    private void uploadCamera(String pushId2) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Uploading...");
        progressDialog.show();

        final String pushId = pushId2;
        final StorageReference ref = storageReference.child(pushId);
        ref.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                Task<Uri> urlTask = ref.putFile(image_uri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }

                        // Continue with the task to get the download URL
                        return ref.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            Upload upload = new Upload(pushId, "ImagePost", downloadUri.toString());
                            DatabaseReference dbRefImageUpload = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage").child(pushId);
                            dbRefImageUpload.setValue(upload);
                            Toast.makeText(getApplicationContext(), downloadUri.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            // Handle failures
                            // ...
                        }
                    }
                });
                Toast.makeText(getApplicationContext(), "Uploaded", Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        progressDialog.setMessage("Uploading...");
                    }
                });
    }

    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        //Camera intent
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE);
    }

    //get image from storage
    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public void takePicture() {
        //system os more then marshmallow, request runtime permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                //show popup to request permission
                requestPermissions(permission, PERMISSION_CODE);
            } else {
                //permission already granted
                openCamera();
            }
        } else {
            //sytem os<marsmallow
            openCamera();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == IMAGE_CAPTURE_CODE && resultCode == RESULT_OK) {
            imageView.setImageURI(image_uri);

        }

        if (requestCode == RECORD_CODE && resultCode == RESULT_OK) {
            audioText.setText("TAP 'PLAY Button' to play audio");
            audioText.setVisibility(View.VISIBLE);
            audioPath = data.getData().toString();
            audioLayout.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), audioPath, Toast.LENGTH_LONG).show();
            audioPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    audioText.setText("Playing...");
                    playAudioFromUrl(audioPath);
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup was granted
                    openCamera();
                } else {
                    //permission from popup was denied
                    Toast.makeText(getApplicationContext(), "Permission denied...", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }
}
