package com.example.andoriddev;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.andoriddev.Extra.Admin;
import com.example.andoriddev.Extra.Premium;
import com.example.andoriddev.Extra.Regular;
import com.example.andoriddev.Extra.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Register functions, takes in name, email (@email.com format is mandatory), password (minimal 6  characters)
 *
 * @author William Rich
 * @version v0.3
 * @since 5 August 2019
 */
public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText name, email, password;
    Button register_btn;
    Spinner spinnerUserType;
    String userType;
    FirebaseAuth auth;
    DatabaseReference reference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);

        Toolbar toolbar = findViewById(R.id.toolbar_register);
        toolbar.setTitle("Registration");
        setSupportActionBar(toolbar);

        name = findViewById(R.id.edittext_name_register);
        email = findViewById(R.id.edittext_email_register);
        password = findViewById(R.id.edittext_password_register);
        register_btn = findViewById(R.id.button_register_register);
        spinnerUserType = findViewById(R.id.spinner_register_user_type);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.user_type, R.layout.spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerUserType.setAdapter(adapter);
        spinnerUserType.setOnItemSelectedListener(this);

        auth = FirebaseAuth.getInstance();

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_name = name.getText().toString();
                String txt_email = email.getText().toString();
                String txt_password = password.getText().toString();

                if (TextUtils.isEmpty(txt_name) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)) {
                    Toast emptyFields = Toast.makeText(RegisterActivity.this, "Please make sure all fields are filled", Toast.LENGTH_LONG);
                    emptyFields.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                    emptyFields.show();
                } else if (txt_password.length() < 6) {
                    Toast shortPassword = Toast.makeText(RegisterActivity.this, "Password must be at least 6 characters long", Toast.LENGTH_LONG);
                    shortPassword.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 1250);
                    shortPassword.show();
                } else {
                    register(txt_name, txt_email, txt_password);
                }
            }
        });
    }

    private void register(final String name, final String email, final String password) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = auth.getCurrentUser();
                    assert firebaseUser != null;
                    String userId = firebaseUser.getUid();
                    reference = FirebaseDatabase.getInstance().getReference("Users").child(userType);
                    Users user = getUsersObject(userId, name, email, password);
                    reference.child(userId).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userType = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public Users getUsersObject(String userId, String nameTxt, String emailTxt, String passwordTxt) {
        Users user;
        switch (userType) {
            case "ADMIN": {
                user = new Admin(userId, nameTxt, emailTxt, passwordTxt);
                return user;
            }
            case "PREMIUM": {
                user = new Premium(userId, nameTxt, emailTxt, passwordTxt);
                return user;
            }
            case "REGULAR": {
                user = new Regular(userId, nameTxt, emailTxt, passwordTxt);
                return user;
            }
        }
        user = new Users();
        return user;
    }
}
