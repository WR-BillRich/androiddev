package com.example.andoriddev.Extra;

public class Comment {
    private String commentID;
    private String commentUser;
    private String postId;
    private String commentContent;

    public Comment() {
    }

    public Comment(String commentID, String commentUser, String postId, String commentContent) {
        this.commentID = commentID;
        this.commentUser = commentUser;
        this.postId = postId;
        this.commentContent = commentContent;
    }

    public String getCommentID() {
        return commentID;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public String getPostId() {
        return postId;
    }

    public String getCommentContent() {
        return commentContent;
    }
}
