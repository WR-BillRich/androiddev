package com.example.andoriddev.Extra;

public class Upload {
    private String postId;
    private String fileType;
    private String fileUrl;

    public Upload() {
    }

    public Upload(String postId, String fileType, String fileUrl) {
        this.postId = postId;
        this.fileType = fileType;
        this.fileUrl = fileUrl;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}
