package com.example.andoriddev.Extra;

import java.io.Serializable;

/**
 * Post.java - an object to hold the post details
 *
 * @author William Rich
 * @version v0.5
 * @since 7 September 2019
 */
public class Post implements Serializable {
    private String postTitle;
    private String postContent;
    private String userId;
    private String name;
    private String postId;
    private String userType;

    public Post() {
    }

    public Post(String postTitle, String postContent, String userId, String name, String postId, String userType) {
        this.postTitle = postTitle;
        this.postContent = postContent;
        this.userId = userId;
        this.name = name;
        this.postId = postId;
        this.userType = userType;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getPostId() {
        return postId;
    }

    public String getUserType() {
        return userType;
    }
}
