package com.example.andoriddev.Extra;

/**
 * Object to hold the user's details
 *
 * @author William Rich
 * @version v0.3
 * @since 5 August 2019
 */

public class Users {
    String userId;
    String name;
    String email;
    String password;

    public Users() {

    }

    public Users(String userId, String name, String email, String password) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean deleteRights() {
        return false;
    }

    public boolean editRights() {
        return false;
    }

    public int priority() {
        return 0;
    }

    ;

    public String getUserType() {
        return "non-specified";
    }
}
