package com.example.andoriddev.Extra;

public class Premium extends Users {
    public Premium() {
    }

    public Premium(String userId, String name, String email, String password) {
        super(userId, name, email, password);
    }

    @Override
    public String getUserId() {
        return super.getUserId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public boolean deleteRights() {
        return true;
    }

    @Override
    public boolean editRights() {
        return true;
    }

    @Override
    public int priority() {
        return 1;
    }

    @Override
    public String getUserType() {
        return "PREMIUM";
    }
}
