package com.example.andoriddev;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.andoriddev.Adapter.Post_List;
import com.example.andoriddev.Extra.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PostListFragment extends Fragment {
    FirebaseAuth auth;
    FirebaseUser firebaseUser;
    ListView listViewPost;
    List<Post> postList;
    DatabaseReference databasePost;

    public static final String KEY_POST_DETAILS = "post_details";

    public PostListFragment() {
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_post_list, container, false);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        listViewPost = (ListView) v.findViewById(R.id.layout_post_list_frag);
        databasePost = FirebaseDatabase.getInstance().getReference("Post List");
        postList = new ArrayList<>();

        if (getActivity() == null) {
            startActivity(new Intent(getContext(), MainActivity.class));
        }

        databasePost.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                postList.clear();

                for (DataSnapshot postDataSnapshot : dataSnapshot.getChildren()) {
                    Post post = postDataSnapshot.getValue(Post.class);
                    postList.add(0, post);
                }
                Post_List listAdapter = new Post_List(getActivity(), postList);
                listViewPost.setAdapter(listAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listViewPost.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Post selectedPost = postList.get(position);
                Bundle bundle = new Bundle();
                bundle.putSerializable("post_details", (Serializable) selectedPost);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                Toast.makeText(getContext(), bundle.getSerializable(KEY_POST_DETAILS).toString(), Toast.LENGTH_SHORT).show();
                if (savedInstanceState == null) {
                    ViewCommentFragment viewCommentFragment = new ViewCommentFragment();
                    viewCommentFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.nested_scroll_frag, viewCommentFragment);
                    fragmentTransaction.commit();
                }

            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event

}
