package com.example.andoriddev;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Forgot password function, takes in email address registered in Firebase Authentication
 * Email containing reset password link will be sent to the email
 *
 * @author William Rich
 * @version v0.4
 * @since 16 August 2019
 */
public class ForgotPasswordActivity extends AppCompatActivity {
    EditText forgotpassword_email;
    Button forgotpassword_reset;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        Toolbar toolbar = findViewById(R.id.toolbar_forgot_activity);
        toolbar.setTitle("Reset Password");
        setSupportActionBar(toolbar);

        forgotpassword_email = findViewById(R.id.edittext_forgotpassword_email);
        forgotpassword_reset = findViewById(R.id.button_forgotpassword_reset);
        firebaseAuth = FirebaseAuth.getInstance();

        forgotpassword_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = forgotpassword_email.getText().toString();
                if (email.equals("")) {
                    Toast.makeText(ForgotPasswordActivity.this, "All Field are Required", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                } else {
                    firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ForgotPasswordActivity.this, "Please Check Your Email", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ForgotPasswordActivity.this, LoginActivity.class));
                            } else {
                                String error = task.getException().getMessage();
                                Toast.makeText(ForgotPasswordActivity.this, error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
    }
}
