package com.example.andoriddev;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.andoriddev.Adapter.Comment_List;
import com.example.andoriddev.Extra.Admin;
import com.example.andoriddev.Extra.Comment;
import com.example.andoriddev.Extra.Post;
import com.example.andoriddev.Extra.Premium;
import com.example.andoriddev.Extra.Regular;
import com.example.andoriddev.Extra.Upload;
import com.example.andoriddev.Extra.Users;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ViewCommmentActivity.java - Activity to allow user view a post and comments
 *
 * @author William Rich
 * @version v0.6
 * @since 8 September 2019
 */
public class ViewCommentActivity extends AppCompatActivity {

    public static final String KEY_POST_DETAILS = "post_details";

    TextView title, content, hostName, audioText;
    EditText commentBox;
    RecyclerView listViewComment;
    List<Comment> commentList;
    FirebaseUser firebaseUser;
    DatabaseReference dbRefComment, dbRefUsers;
    FloatingActionButton fabComment, audioPlay, audioStop;
    ImageView imageView;
    LinearLayout audioLayout;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_comment);

        Toolbar toolbar = findViewById(R.id.toolbar_view_comment);
        toolbar.setTitle("View and Comment");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        hostName = (TextView) findViewById(R.id.textview_view_comment_host_user_name);
        title = (TextView) findViewById(R.id.textview_view_comment_title);
        content = (TextView) findViewById(R.id.textview_view_comment_content);
        commentBox = (EditText) findViewById(R.id.edittext_view_comment_comment);
        listViewComment = (RecyclerView) findViewById(R.id.listview_view_comment_comment);
        fabComment = (FloatingActionButton) findViewById(R.id.fab_comment);
        commentList = new ArrayList<>();
        imageView = (ImageView) findViewById(R.id.imageView_view_comment_picture);

        dbRefComment = FirebaseDatabase.getInstance().getReference("Comment");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        dbRefUsers = FirebaseDatabase.getInstance().getReference("Users");

        //Audio
        audioText = (TextView) findViewById(R.id.textview_view_comment_audio);
        audioPlay = (FloatingActionButton) findViewById(R.id.fab_audio_play);
        audioStop = (FloatingActionButton) findViewById(R.id.fab_audio_stop);
        audioLayout = (LinearLayout) findViewById(R.id.audio_layout);


        final Post post = (Post) getIntent().getSerializableExtra(KEY_POST_DETAILS);

        title.setText(post.getPostTitle());
        content.setText(post.getPostContent());
        hostName.setText("@" + post.getName());

        fabComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentStr = commentBox.getText().toString();
                Toast.makeText(getApplicationContext(), "clicked fab", Toast.LENGTH_SHORT).show();
                if (commentStr.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "No comment", Toast.LENGTH_LONG).show();
                } else {
                    dbRefUsers.child("REGULAR").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                                Users users = userDetailsSnapshot.getValue(Regular.class);
                                if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                                    String commentStr = commentBox.getText().toString();
                                    String postID = post.getPostId();
                                    String commentUser = users.getName().toString();
                                    String pushId = dbRefComment.push().getKey().toString();
                                    Comment comment = new Comment(pushId, commentUser, postID, commentStr);
                                    dbRefComment.child(post.getPostId().toString()).child(pushId).setValue(comment);
                                    commentBox.getText().clear();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    dbRefUsers.child("PREMIUM").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                                Users users = userDetailsSnapshot.getValue(Premium.class);
                                if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                                    String commentStr = commentBox.getText().toString();
                                    String postID = post.getPostId();
                                    String commentUser = users.getName().toString();
                                    String pushId = dbRefComment.push().getKey().toString();
                                    Comment comment = new Comment(pushId, commentUser, postID, commentStr);
                                    dbRefComment.child(post.getPostId().toString()).child(pushId).setValue(comment);
                                    commentBox.getText().clear();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    dbRefUsers.child("ADMIN").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot userDetailsSnapshot : dataSnapshot.getChildren()) {
                                Users users = userDetailsSnapshot.getValue(Admin.class);
                                if (users.getUserId().equals(firebaseUser.getUid().toString())) {
                                    String commentStr = commentBox.getText().toString();
                                    String postID = post.getPostId();
                                    String commentUser = users.getName().toString();
                                    String pushId = dbRefComment.push().getKey().toString();
                                    Comment comment = new Comment(pushId, commentUser, postID, commentStr);
                                    dbRefComment.child(post.getPostId().toString()).child(pushId).setValue(comment);
                                    commentBox.getText().clear();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        dbRefComment.child(post.getPostId().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                commentList.clear();
                for (DataSnapshot commentDataSnapshot : dataSnapshot.getChildren()) {
                    Comment comment = commentDataSnapshot.getValue(Comment.class);
                    commentList.add(comment);
                }
                Comment_List listAdapter = new Comment_List(ViewCommentActivity.this, commentList);
                listViewComment.setAdapter(listAdapter);
                listViewComment.setLayoutManager(new LinearLayoutManager(ViewCommentActivity.this));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        getImageUrl(post);
        getAudioUrl(post);

        audioStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    audioText.setText("TAP 'PLAY' to play audio");
                }
            }
        });


    }

    private void getImageUrl(final Post post) {
        DatabaseReference dbRefUrl = FirebaseDatabase.getInstance().getReference("Uploads").child("PostImage");
        dbRefUrl.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot imageDataSnapshot : dataSnapshot.getChildren()) {
                    Upload uploadChild = imageDataSnapshot.getValue(Upload.class);
                    if (post.getPostId().equals(uploadChild.getPostId())) {
                        setImageFromUrl(uploadChild.getFileUrl());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setImageFromUrl(String fileUrl) {
        Picasso.get().load(fileUrl)
                .error(R.mipmap.ic_launcher)
                .into(imageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
    }

    private void getAudioUrl(final Post post) {
        DatabaseReference dbRefUrl = FirebaseDatabase.getInstance().getReference("Uploads").child("PostAudio");
        dbRefUrl.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot audioDataSnapshot : dataSnapshot.getChildren()) {
                    Upload uploadChild = audioDataSnapshot.getValue(Upload.class);
                    if (post.getPostId().equals(uploadChild.getPostId())) {
                        final String audioUrl = uploadChild.getFileUrl();
                        audioLayout.setVisibility(View.VISIBLE);
                        audioText.setText("TAP 'PLAY' to play audio");
                        audioPlay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                audioText.setText("Playing...");
                                playAudioFromUrl(audioUrl);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void playAudioFromUrl(String fileUrl) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(fileUrl);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    audioText.setText("TAP 'PLAY' to play audio");
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void emptyWarning() {
        androidx.appcompat.app.AlertDialog.Builder y = new androidx.appcompat.app.AlertDialog.Builder(getApplicationContext());

        y.setTitle("EMPTY VALUE");
        y.setMessage("No comment added");
        y.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = y.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }
}
