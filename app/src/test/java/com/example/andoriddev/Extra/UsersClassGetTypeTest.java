package com.example.andoriddev.Extra;

import com.firebase.ui.auth.data.model.User;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsersClassGetTypeTest {

    //Get User Type
    @Test
    public void testGetUsersType() {
        Users users = new Users();
        assertEquals("non-specified",users.getUserType());
    }

    @Test
    public void testGetRegularType() {
        Users regular = new Regular();
        assertEquals("REGULAR",regular.getUserType());
    }

    @Test
    public void testGetPremiumType() {
        Users premium = new Premium();
        assertEquals("PREMIUM",premium.getUserType());
    }

    @Test
    public void testGetAdminType() {
        Users admin = new Admin();
        assertEquals("ADMIN",admin.getUserType());
    }
}