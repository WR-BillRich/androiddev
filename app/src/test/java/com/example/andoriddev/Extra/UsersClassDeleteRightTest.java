package com.example.andoriddev.Extra;

import com.firebase.ui.auth.data.model.User;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsersClassDeleteRightTest {

    //Delete Rights
    @Test
    public void testDeleteRightsUsersSuccess() {
        Users user1 = new Users();
        assertEquals(false,user1.deleteRights());
    }

    @Test
    public void testDeleteRightsRegularSuccess() {
        Users regular = new Regular();
        assertEquals(true,regular.deleteRights());
    }


    @Test
    public void testDeleteRightsPremiumSuccess() {
        Users premium = new Premium();
        assertEquals(true,premium.deleteRights());
    }


    @Test
    public void testDeleteRightsAdminSuccess() {
        Users premium = new Admin();
        assertEquals(true,premium.deleteRights());
    }
}