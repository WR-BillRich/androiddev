package com.example.andoriddev.Extra;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UsersClassEditRightTest {
    //Edit Rights
    @Test
    public void testEditRightsUsersSuccess() {
        Users user1 = new Users();
        assertEquals(false, user1.editRights());
    }


    @Test
    public void testEditRightsRegularSuccess() {
        Users regular = new Regular();
        assertEquals(false, regular.editRights());
    }


    @Test
    public void testEditRightsPremiumSuccess() {
        Users premium = new Premium();
        assertEquals(true, premium.editRights());
    }


    @Test
    public void testEditRightsAdminSuccess() {
        Users premium = new Premium();
        assertEquals(true, premium.editRights());
    }


}