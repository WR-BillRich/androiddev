package com.example.andoriddev.Extra;

import com.firebase.ui.auth.data.model.User;

import org.junit.Test;

import static org.junit.Assert.*;

public class UsersClassPriorityTest {

    //Priority
    @Test
    public void testUsersPriority() {
        Users users = new Users();
        assertEquals(0,users.priority());
    }

    @Test
    public void testRegularPriority() {
        Users regular = new Regular();
        assertEquals(1,regular.priority());
    }

    @Test
    public void testPremiumPriority() {
        Users premium = new Premium();
        assertEquals(1,premium.priority());
    }

    @Test
    public void testAdminPriority() {
        Users admin = new Admin();
        assertEquals(2,admin.priority());
    }

}